<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes" indent="yes"/>
  <xsl:param name="fullHTML">true</xsl:param>

  <xsl:template name="repNL">
    <xsl:param name="pText"/>

    <xsl:copy-of select="substring-before(concat($pText,'&#xA;&#xA;'),'&#xA;&#xA;')"/>

    <xsl:if test="contains($pText, '&#xA;&#xA;')">
      <br />
      <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
      <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
      <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
      <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
      <xsl:call-template name="repNL">
        <xsl:with-param name="pText" select="substring-after($pText, '&#xA;&#xA;')"/>
      </xsl:call-template>
    </xsl:if>

  </xsl:template>

  <xsl:template match="/">
    <xsl:variable name="innerHTML">

      <!--<h2>Journals</h2>
      <ul class="publications">
        <xsl:apply-templates select="publications/publication[@type='journal']" />
      </ul>-->

      <h2>Conferences</h2>
      <ul class="publications">
        <xsl:apply-templates select="publications/publication[@type='conference']" />
      </ul>

    </xsl:variable>

    <xsl:if test="$fullHTML != 'false'">
      <html>
        <head>
          <title>Publications</title>
          <link rel="stylesheet" type="text/css" href="style.css" />
          <link rel="stylesheet" type="text/css" href="../style.css" />
        </head>
        <body>
          <h1>Publications</h1>
          <xsl:copy-of select="$innerHTML" />
        </body>
      </html>
    </xsl:if>
    <xsl:if test="$fullHTML = 'false'">
      <xsl:copy-of select="$innerHTML" />
    </xsl:if>
  </xsl:template>

  <xsl:template match="publication">
    <li class="publication">
      <div class="shortname">
        [<xsl:value-of select="shortname" />]
      </div>
      <div class="publication">
        <!-- comma-separated list of authors -->
        <span class="authors">
          <xsl:for-each select="author">
            <xsl:if test="@href">
              <!-- link to author's home page -->
              <a href="{@href}">
                <!-- author's name -->
                <xsl:value-of select="." />
              </a>
            </xsl:if>
            <xsl:if test="not(@href)">
              <!-- there is no link - just show the name -->
              <xsl:value-of select="." />
            </xsl:if>
            <xsl:if test="position() &lt; last() - 1">
              <xsl:text>, </xsl:text>
            </xsl:if>
            <xsl:if test="position() = last() - 1">
              <xsl:text> and </xsl:text>
            </xsl:if>
            <xsl:if test="position() = last()">
              <xsl:text>. </xsl:text>
            </xsl:if>
          </xsl:for-each>
        </span>

        <!-- title -->
        <span class="title">
         <xsl:if test="a/@default">
            <a href="{a/@default/../@href}">
              <xsl:value-of select="title" />.
            </a>
          </xsl:if>
          <xsl:if test="not(a/@default)">
            <xsl:value-of select="title" />.
          </xsl:if>
        </span>

        <!-- if this is a journal, the journal title -->
        <xsl:if test="@type = 'journal'">
          <span class="jtitle">
            <xsl:value-of select="journal" />
          </span>
          <span class="jtitle_pages">
            <xsl:text> </xsl:text>
            <xsl:value-of select="volume" />
            <xsl:if test="number">
              <xsl:text>(</xsl:text><xsl:value-of select="number" /><xsl:text>)</xsl:text>
            </xsl:if>
            <xsl:if test="pages">
              <xsl:text>, pages </xsl:text><xsl:value-of select="pages" />
            </xsl:if>
            <xsl:text>, </xsl:text><xsl:value-of select="year" />
            <xsl:text>.</xsl:text>
          </span>
        </xsl:if>

        <!-- if this is in conference proceedings, the book title -->
        <xsl:if test="@type = 'conference'">
          <span class="book">
            <xsl:variable name="book_info">
              <span class="book_title">
                <xsl:value-of select="book" />
              </span>
              <span class="book_pages">
		<xsl:if test="pages">
                  <xsl:text>, pages </xsl:text><xsl:value-of select="pages" />
		</xsl:if>
	      </span>
            </xsl:variable>

            <span class="in"><xsl:text> In </xsl:text></span>
            <xsl:if test="book/@href">
              <a href="{book/@href}">
                <xsl:copy-of select="$book_info" />
              </a>
            </xsl:if>
            <xsl:if test="not(book/@href)">
              <xsl:copy-of select="$book_info" />
            </xsl:if>

            <xsl:text> </xsl:text><xsl:value-of select="publisher" />
            <xsl:text>, </xsl:text><xsl:value-of select="year" />
            <xsl:text>. </xsl:text>
          </span>
        </xsl:if>

        <!-- publication type -->
        <xsl:if test="type">
          <span class="pubtype">
            <xsl:text> </xsl:text><xsl:value-of select="type" />
            <xsl:text>,</xsl:text>
          </span>
        </xsl:if>

        <!-- school name -->
        <xsl:if test="school">
          <span class="school">
            <xsl:text> </xsl:text><xsl:value-of select="school" />
            <xsl:text>,</xsl:text>
          </span>
        </xsl:if>

        <!-- institution -->
        <xsl:if test="institution">
          <span class="institution">
            <xsl:text>Research report, </xsl:text><xsl:value-of select="institution" />
            <xsl:text>,</xsl:text>
          </span>
        </xsl:if>

        <!-- date (when not next to publisher) -->
        <xsl:if test="@type != 'journal' and @type != 'conference'">
          <span class="date">
            <xsl:text> </xsl:text>
            <xsl:if test="month">
              <xsl:value-of select="month" /><xsl:text> </xsl:text>
            </xsl:if>
            <xsl:value-of select="year" /><xsl:text>.</xsl:text>
          </span>
        </xsl:if>

        <xsl:variable name="abstract_id">
          <xsl:value-of select="shortname" />
        </xsl:variable>

        <span class="links">

          <!-- Show all the links -->
          <xsl:for-each select="a">
	    <!--
            <xsl:if test="position( ) = 1">
              <xsl:text> ( </xsl:text>
            </xsl:if>
	    -->
            <a href="{@href}">
              <xsl:value-of select="." />
            </a>
            <xsl:if test="position() != last()">
              <xsl:text> | </xsl:text>
            </xsl:if>
	    <!--
            <xsl:if test="position() = last()">
              <xsl:text> ) </xsl:text>
            </xsl:if>
	    -->
          </xsl:for-each>

          <!-- The abstract -->

          <xsl:if test="abstract">
            <xsl:text> | </xsl:text>
            <a onclick ="javascript:ShowHide('{$abstract_id}')" href="javascript:;" >Abstract</a>
          </xsl:if>
        </span>

          <xsl:if test="abstract">

            <div class="mid" id="{$abstract_id}" style="DISPLAY: none; border:1px dashed gray; margin-top: 0.5cm; padding:15px; text-indent: 2em;" >

              <xsl:call-template name="repNL">
                <xsl:with-param name="pText" select="abstract" />

              </xsl:call-template>


            </div>
          </xsl:if>
      </div>
    </li>
  </xsl:template>

</xsl:stylesheet>

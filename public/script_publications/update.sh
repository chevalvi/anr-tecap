#!/bin/sh
# Usage: ./update.sh
# Run me to update the publications list from publications.xml

xsltproc --stringparam fullHTML false publications.xsl publications.xml |  # XSL transform
  grep -v '<?xml' > output.xml ||  # remove XML declaration
  return $?
echo "Written output.xml"
sed -i '' s/"é"/"\&eacute;"/g output.xml
sed -i '' s/"à"/"\&agrave;"/g output.xml
sed -i '' s/"â"/"\&acirc;"/g output.xml
sed -i '' s/"ç"/"\&ccedil;"/g output.xml
sed -i '' s/"è"/"\&egrave;"/g output.xml
sed -i '' s/"ê"/"\&ecirc;"/g output.xml
sed -i '' s/"ù"/"\&ugrave;"/g output.xml
sed -i '' s/"û"/"\&ucirc;"/g output.xml
echo "Replacement of accents"
